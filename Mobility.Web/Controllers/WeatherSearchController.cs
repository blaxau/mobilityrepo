using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Mobility.Services.Models;
using Mobility.Services;
using Microsoft.AspNetCore.Mvc;

namespace Mobility.Controllers
{
    [Route("api/[controller]")]
    public class WeatherSearchController : Controller
    {
        [HttpGet("[action]")]
        public IWeatherSearchInfo Search([FromServices]IWeatherSearchService service, string city, string state)
        {
            return service.Search(city, state);
        }
    }
}
