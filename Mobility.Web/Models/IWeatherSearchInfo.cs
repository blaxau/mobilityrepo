﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Mobility.Services.Models
{
    public interface IWeatherSearchInfo
    {
        IWeatherSeachTempInfo TempInfo { get; set; }
        string ErrorMessage { get; set; }
    }
   
    public interface IWeatherSeachTempInfo
    {
        decimal TempMain { get; set; }
        decimal TempFeelsLike { get; set; }
        decimal TempMin { get; set; }
        decimal TempMax { get; set; }
        decimal Humidity { get; set; }
    }
}
