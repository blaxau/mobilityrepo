﻿using RestSharp.Deserializers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Mobility.Services.Models
{
    public class OpenWeatherMapInfo : IWeatherSearchInfo
    {
        private OpenWeatherTempInfo _tempInfo = null;
        public OpenWeatherTempInfo Main { get => (_tempInfo == null ? _tempInfo = new OpenWeatherTempInfo() : _tempInfo); set => _tempInfo = value; }
        public string ErrorMessage { get; set; }
        IWeatherSeachTempInfo IWeatherSearchInfo.TempInfo { get => Main; set => Main = (OpenWeatherTempInfo)value; }
    }

    public class OpenWeatherTempInfo : IWeatherSeachTempInfo
    {
        [DeserializeAs(Name = "temp")]
        public decimal TempMain { get; set; }

        [DeserializeAs(Name = "feels_like")]
        public decimal TempFeelsLike { get; set; }

        [DeserializeAs(Name = "temp_min")]
        public decimal TempMin { get; set; }
        [DeserializeAs(Name = "temp_max")]
        public decimal TempMax { get; set; }
        [DeserializeAs(Name = "humidity")]
        public decimal Humidity { get; set; }
    }
}
