import { async, TestBed, inject } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';

import { WeatherSearchService } from './WeatherSearch.service';

describe('WeatherSearchService', () => {

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [WeatherSearchService]
    })
  }));

  it('should not break on null',
    inject(
      [HttpTestingController, WeatherSearchService],
      (httpMock: HttpTestingController, searchService: WeatherSearchService) => {
        searchService.runSearch(null, null).subscribe(r => expect(r != null))
      })
  );
  it('should not break on empty string',
    inject(
      [HttpTestingController, WeatherSearchService],
      (httpMock: HttpTestingController, searchService: WeatherSearchService) => {
        searchService.runSearch("", "").subscribe(r => expect(r != null))
      })
  );

  it('should work with only city',
    inject(
      [HttpTestingController, WeatherSearchService],
      (httpMock: HttpTestingController, searchService: WeatherSearchService) => {
        searchService.runSearch("Perth", "").subscribe(r => expect(r.main != null))
      })
  );


  it('should work with city and state',
    inject(
      [HttpTestingController, WeatherSearchService],
      (httpMock: HttpTestingController, searchService: WeatherSearchService) => {
        searchService.runSearch("Perth", "Western Australia").subscribe(r => expect(r.main != null))
      })
  );

  it('should break with invalid state',
    inject(
      [HttpTestingController, WeatherSearchService],
      (httpMock: HttpTestingController, searchService: WeatherSearchService) => {
        searchService.runSearch("Perth", "wa").subscribe(r => expect(r.errorMessage != null))
      })
  );

});
