import { Injectable } from '@angular/core';
import { HttpParams, HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { environment } from '../../environments/environment';
import { WeatherSearchInfo } from './weathersearchinfo';

@Injectable()

export class WeatherSearchService {
  constructor(private http: HttpClient) { }

  runSearch(city: string, state: string): Observable<WeatherSearchInfo> {
    let params = new HttpParams();
    params = params.append('city', city);
    params = params.append('state', state);
    
    return this.http.get<WeatherSearchInfo>(environment.BASE_URL + 'api/WeatherSearch/Search', { params: params });
  }
}

