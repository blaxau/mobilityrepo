import { Component } from '@angular/core';
import { WeatherSearchService } from './WeatherSearch.service';
import { WeatherSearchInfo } from './weathersearchinfo';

@Component({
  selector: 'WeatherSearch',
  templateUrl: './WeatherSearch.component.html'
})
export class WeatherSearchComponent {
  public disableSearch: boolean;
  public weatherSearchInfo: WeatherSearchInfo;
  public errorMessage: string;

  constructor(public weatherSearchService: WeatherSearchService)
  {
    this.disableSearch = true;
  }

  searchOnClick() {
    let city = (<HTMLInputElement>document.getElementById('txtCity')).value;
    let state = (<HTMLInputElement>document.getElementById('txtState')).value;
    this.runSearch(city, state);
  }

  runSearch(city: string, state: string) {
    this.errorMessage = "";
    this.weatherSearchInfo = null;    
    this.weatherSearchService
      .runSearch(city, state)
      .subscribe
      (
        searchResult => {
          if (searchResult.errorMessage == null || searchResult.errorMessage === "") {
            this.weatherSearchInfo = searchResult;
            let lsKey = city;
            if (state != null && state != "")
              lsKey = lsKey + "," + state;
            if (localStorage.getItem(lsKey) == null) {
              localStorage.setItem(lsKey, lsKey);
            }
          }
          else {
            this.errorMessage = searchResult.errorMessage;
          }
        }
      );
  }

  get getLsItems(): string[] {
    return Object.keys(localStorage);
  }

  onCityChanged(txtCity: string) {
    if (txtCity === "")
      this.disableSearch = true;
    else
      this.disableSearch = false;
  }

  weatherOnClick(lsKey: string) {
    let city = "";
    let state = "";
    if (lsKey.indexOf(",") > 0) {
      city = lsKey.split(",")[0];
      state = lsKey.split(",")[1];
    }
    else
      city = lsKey;
    (<HTMLInputElement>document.getElementById('txtCity')).value = city;
    (<HTMLInputElement>document.getElementById('txtState')).value = state;
    this.runSearch(city, state);
  }

  weatherRemoveOnClick(lsKey: string) {
    localStorage.removeItem(lsKey);
  }

}

