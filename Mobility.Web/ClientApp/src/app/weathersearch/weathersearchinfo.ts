export interface WeatherSearchInfo {
  main: Main;
  errorMessage: string;
}

export interface Main {
  tempMain: number;
  tempFeelsLike: number;
  tempMin: number;
  tempMax: number;
  humidity: number;
}
