import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { RouterModule } from '@angular/router';

import { AppComponent } from './app.component';
import { NavMenuComponent } from './nav-menu/nav-menu.component';
import { WeatherSearchComponent } from './WeatherSearch/WeatherSearch.component';

import { WeatherSearchService } from './WeatherSearch/WeatherSearch.service';

@NgModule({
  declarations: [
    AppComponent,
    NavMenuComponent,
    WeatherSearchComponent
  ],
  imports: [
    BrowserModule.withServerTransition({ appId: 'ng-cli-universal' }),
    HttpClientModule,
    FormsModule,
    RouterModule.forRoot([
      { path: '', component: WeatherSearchComponent, pathMatch: 'full' }
    ])
  ],
  providers: [WeatherSearchService],
  bootstrap: [AppComponent]
})
export class AppModule { }
