﻿using Mobility.Services.Models;
using RestSharp;
using System;

namespace Mobility.Services
{
    public interface IWeatherSearchService
    {
        IWeatherSearchInfo Search(string city, string state);
    }
    public class OpenWeatherMapSearchService : IWeatherSearchService
    {
        IWeatherSearchInfo IWeatherSearchService.Search(string city, string state)
        {
            return new ThirdParty.OpenWeatherMap().GetResponse(city, state, ThirdParty.Uom.Celcius);
        }
    }
}
