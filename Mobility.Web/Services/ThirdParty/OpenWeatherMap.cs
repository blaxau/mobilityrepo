﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Mobility.Services.Models;
using RestSharp;

namespace Mobility.Services.ThirdParty
{
    public class OpenWeatherMap : Base<OpenWeatherMapInfo>
    {
        private Uri GetUri => new Uri("http://api.openweathermap.org/data/2.5/weather");
        private string APIKey => "ed3ccea1ffc116c027d5335d7d74e287";

        public OpenWeatherMap(){ }

        public override OpenWeatherMapInfo GetResponse(string city, string state, Uom uom)
        {
            IRestClient client = new RestClient(GetUri);
            IRestRequest request = new RestRequest();
            client.AddDefaultQueryParameter("q", city + (string.IsNullOrEmpty(state) ? "" : "," + state));
            client.AddDefaultQueryParameter("appid", APIKey);

            IRestResponse<OpenWeatherMapInfo> response = client.Execute<OpenWeatherMapInfo>(request);

            if (response.IsSuccessful)
            {
                //default returns Kelvin so modify the other 2 types
                switch(uom)
                {
                    case Uom.Celcius:
                        response.Data.Main.TempMain = response.Data.Main.TempMain.KelvinToCelcius();
                        response.Data.Main.TempFeelsLike = response.Data.Main.TempFeelsLike.KelvinToCelcius();
                        response.Data.Main.TempMax = response.Data.Main.TempMax.KelvinToCelcius();
                        response.Data.Main.TempMin = response.Data.Main.TempMin.KelvinToCelcius();
                        break;
                    case Uom.Fahrenheit:
                        response.Data.Main.TempMain = response.Data.Main.TempMain.KelvinToFahrenheit();
                        response.Data.Main.TempFeelsLike = response.Data.Main.TempFeelsLike.KelvinToFahrenheit();
                        response.Data.Main.TempMax = response.Data.Main.TempMax.KelvinToFahrenheit();
                        response.Data.Main.TempMin = response.Data.Main.TempMin.KelvinToFahrenheit();
                        break;
                }
                return response.Data;
            }
            else
            {
                dynamic details = Newtonsoft.Json.Linq.JObject.Parse(response.Content);
                return new OpenWeatherMapInfo { ErrorMessage = details.message.ToString() };
            }
        }
        
    }
}
