﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Mobility.Services.ThirdParty
{
    public static class Extensions
    {
        public static decimal KelvinToCelcius(this decimal K)
        {
            return (K - 273.15M);
        }
        public static decimal KelvinToFahrenheit(this decimal K)
        {
            return ((((K - 273.15M) * 9) / 5) + 32);
        }
    }
}
