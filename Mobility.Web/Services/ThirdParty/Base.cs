﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Mobility.Services.ThirdParty
{
    public enum Uom
    {
        Kelvin,
        Celcius,
        Fahrenheit
    }
    public abstract class Base<T>
    {       
        abstract public T GetResponse(string city, string state, Uom uom);
    }
}
