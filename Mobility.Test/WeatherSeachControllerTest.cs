using System.Linq;
using Xunit;
using Mobility.Controllers;
using Mobility.Services;

namespace Mobility.Test.Controllers
{
    public class WeatherSearchControllerTest
    {
        private readonly IWeatherSearchService _service;

        public WeatherSearchControllerTest()
        {
            _service = new OpenWeatherMapSearchService();
        }

        [Fact]
        public void EmptyTest()
        {
            var result = _service.Search(null, null);
            Assert.NotNull(result.ErrorMessage);
        }
        [Fact]
        public void SingleTest()
        {
            var result = _service.Search("Perth", null);
            Assert.NotNull(result.TempInfo);
        }
        [Fact]
        public void MultipleTest()
        {
            var result = _service.Search("Perth", "western australia");
            Assert.NotNull(result.TempInfo);
        }
        [Fact]
        public void InvalidTest()
        {
            var result = _service.Search("Perth", "wa");
            Assert.NotNull(result.ErrorMessage);
        }

    }
}
